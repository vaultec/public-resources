#!/bin/bash
#The scoring system for dtube IPFS full node contest. Mock up

testhash="QmdFYag2tRi3au9zGXRi6ewmJY5oFdrYDyTo1ibHrR8Ca9"
#Speed test for public gateways.
curl -s https://gitlab.com/vaultec/public-resources/raw/master/IPFS/public_gateways.txt | while read gateway
do
   (time curl -o /dev/null $gateway/$testhash) &>> results.txt #Fix stderr output. custom command.
   (time curl $gateway/api/v0/dag/get?arg=$testhash) &>> results.txt
done

#T
echo "Running ipfs ping test" >> results.txt
curl -s https://gitlab.com/vaultec/public-resources/raw/master/IPFS/node_list.txt | while read node
do
    id="$(cut -d'/' -f7 <<<"$node")"
    echo "[ping test] node: $id" >> results.txt
    ipfs ping -n 4 $node >> results.txt
    echo "Raw ping"
    node_ip="$(cut -d'/' -f3 <<<"$node")"
    ping -c 4 node_ip >> results.txt #Raw ping
done
