#!/bin/bash
cd /tmp
wget https://github.com/ipfs/go-ipfs/releases/download/v0.4.19/go-ipfs_v0.4.19_linux-amd64.tar.gz #Wget should be already installed
tar -xf go-ipfs_v0.4.19_linux-amd64.tar.gz #Should already be installed
cd go-ipfs
sudo ./install.sh
ipfs init