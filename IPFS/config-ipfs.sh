#!/bin/bash

if ! [ -x "$(command -v ipfs)" ]; then
  echo 'Error: ipfs is not installed.' >&2
  exit 1
fi

echo "Using recommended settings!"
ipfs config --json Swarm.ConnMgr.LowWater 25
ipfs config --json Swarm.ConnMgr.HighWater 75
ipfs config --json Swarm.ConnMgr.GracePeriod 10s

echo "QUIC transport is a faster, more reliable, UDP based protocol. Used in HTTPS"
echo "Would you like to enable QUIC transport? [y/n]"
read $var
if [ "$var" == "y" ] ;then
    echo "Enabling QUIC transport"
    ipfs config --json Experimental.QUIC true
else
   echo "Not enabling quic transport!"
fi

echo "Maximum IPFS storage usage? (In GB, number only)"
read $var1
if [ -n "$var1" ]; then
 ipfs config --json Datastore.StorageMax $var1"GB"
 else 
 echo "No maximum storage amount specified"
fi

echo "IPFS keepalive is a script that keeps IPFS connected to the public node list."
echo "Would you like to enable keepalive? (runs in crontab) [y/n]"
read $var
if [ "$var" == "y" ] ;then
    echo "Enabling QUIC transport"
    ipfs config --json Experimental.QUIC true
else
   echo "Not enabling quic transport!"
fi

echo "configuration completed. You can now start/restart IPFS!"
exit 1;
