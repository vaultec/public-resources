# IPFS 

#### General
To install IPFS <br>
`curl -L https://bit.ly/2EbhZh7 | sh`

Start IPFS with screen and exit. <br>
With pubsub (recommended) <br>
`screen -d -m "ipfs daemon --enable-pubsub-experiment"` <br>
without pubsub <br>
`screen -d -m "ipfs daemon"`

#### Keepalive
Keep alive script, tells IPFS continueously to stay connected to the public node list. <br>
`curl -L https://bit.ly/2BWdGps | sh`

#### Config IPFS 
Automated configure IPFS, no editing files needed! <br>
`curl -L https://bit.ly/2tDTSCM | sh`
